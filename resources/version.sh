#!/bin/bash

# Increment a version string using Semantic Versioning terminology.

# Parse command line options.

while getopts ":MmPp" Option
do
  case $Option in
    M ) major=true;;
    m ) minor=true;;
    P ) patch=true;;
    p ) prelease=true;;
  esac
done

# If a flag is missing, show usage message.
if [ $OPTIND -eq 1 ]; then
  echo "No options were passed";
  echo "usage: $(basename $0) [-MmPp] major.minor.patch-prerelease"
  exit 1
fi

shift $(($OPTIND - 1))

version=$1

if [ -z $version ]; then
  version="0.0.0"
fi

# Build array from version string.
p=( ${version//-/ } )
version=${p[0]}
if [ ! -z ${p[1]} ]; then
  p[1]="SNAPSHOT"
fi

a=( ${version//./ } )

# Increment version numbers as requested.
if [ ! -z $major ]; then
  ((a[0]++))
  a[1]=0
  a[2]=0
  p[1]=""
fi

if [ ! -z $minor ]; then
  ((a[1]++))
  a[2]=0
  p[1]=""
fi

if [ ! -z $patch ]; then
  if [ ! -z ${p[1]} ]; then
    p[1]=""
  else
    ((a[2]++))
  fi
fi


if [ ! -z $prelease ]; then
  if [ -z ${p[1]} ]; then
    ((a[2]++))
    p[1]="SNAPSHOT"
  fi
fi


if [ ! -z ${p[1]} ]; then
  echo "${a[0]}.${a[1]}.${a[2]}-${p[1]}"
else
  echo "${a[0]}.${a[1]}.${a[2]}"
fi