include:
  - project: 'nara-way/devops/gitops'
    file:
      - 'gitlab-ci/naraway-common-ci.yml'

variables:
  APPS_PATH: "apps"

stages:
  - build
  - deploy

build-package:
  stage: build
  script:
    - echo -e "${LOG_HEADER}Building package...${LOG_FOOTER}"
    - VERSION="4.0.1"
    - TAG_VERSION=${VERSION}.${CI_PIPELINE_ID}
    - echo "BUILD VERSION":"${VERSION}"
    - echo "TAG_VERSION":"${TAG_VERSION}"

    - if [ ! "$NPMRC" = "default" ]; then curl --header PRIVATE-TOKEN:${GITLAB_CI_TOKEN} ${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}:${CI_SERVER_PORT}/api/v4/projects/${GITOPS_PROJECT_ID}/repository/files/gitlab-ci%2Fresources%2F.npmrc.${NPMRC}/raw?ref=main > .npmrc; fi
    - if [ ! "$NPMRC" = "default" ]; then if [ -f yarn.lock ]; then rm yarn.lock; fi; fi
    - yarn install
    - yarn build

    - echo -e "${LOG_HEADER}Building docker image...${LOG_FOOTER}"
    - curl --header PRIVATE-TOKEN:${GITLAB_CI_TOKEN} ${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}:${CI_SERVER_PORT}/api/v4/projects/${GITOPS_PROJECT_ID}/repository/files/gitlab-ci%2Fresources%2Fnginx%2FDockerfile.template/raw?ref=main > Dockerfile
    - curl --header PRIVATE-TOKEN:${GITLAB_CI_TOKEN} ${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}:${CI_SERVER_PORT}/api/v4/projects/${GITOPS_PROJECT_ID}/repository/files/gitlab-ci%2Fresources%2Fnginx%2Fdefault.conf/raw?ref=main > default.conf
    - |
      for APP in $(echo ${APPS} | sed 's/,/ /g')
      do
        if [ -n ${APP} ]
        then
          echo "APP":"${APP}"
          APP_IMAGE_TAG="${DOCKER_IMAGE_TAG}/${APPS_PATH}/${APP}"
          docker build -t "${APP_IMAGE_TAG}":"${TAG_VERSION}" --build-arg APP="${APP}" -f ./Dockerfile .
          docker login -u "${REGISTRY_USER}" -p "${REGISTRY_PASSWORD}" "${IMAGE_REGISTRY}"
          if [ "$DRY_RUN" = "true" ]
          then
            echo -e "${LOG_HEADER}--- Dry Run mode: 이미지 푸쉬 생략 됨...${LOG_FOOTER}"
          else
            docker push "${APP_IMAGE_TAG}":"${TAG_VERSION}"
          fi
          docker image rm "${APP_IMAGE_TAG}":"${TAG_VERSION}"
          echo "Pushed image":"${APP_IMAGE_TAG}":"${TAG_VERSION}"
        fi
      done
  artifacts:
    paths:
      - "dramas/**/dist/"
      - "apps/**/dist/"
    expire_in: 30 min
  rules:
    - if: $ENVIRONMENT == "dev"
      when: on_success
    - if: $ENVIRONMENT == "prd"
      when: on_success

update-gitops-repository:
  stage: deploy
  before_script:
    - STR_ARRAY=(`echo $GITOPS_URL | tr "/" "\n"`); STR_ARRAY=(`echo ${STR_ARRAY[-1]} | tr "." "\n"`)
    - GITOPS_PROJECT_NAME=(`echo ${STR_ARRAY[0]}`)
  script:
    - VERSION="4.0.1"
    - TAG_VERSION=${VERSION}.${CI_PIPELINE_ID}
    - echo "TAG_VERSION":"${TAG_VERSION}"

    - echo -e "${LOG_HEADER}Cloing GitOps repository...${LOG_FOOTER}"
    - git clone ${GITOPS_URL}

    - echo -e "${LOG_HEADER}Updating k8s manifests...${LOG_FOOTER}"
    - cd ${GITOPS_PROJECT_NAME}
    - |
      for APP in $(echo ${APPS} | sed 's/,/ /g')
      do
        if [ -n ${APP} ]
        then
          echo "APP":"${APP}"
          DEPLOYMENT_FILE="${MANIFESTS_PATH}/${APPS_PATH}/${APP}/deployment.yml"
          sed -i "s/^\(          image\s*:\s*\).*\$/\1$IMAGE_REGISTRY\/$(echo $CI_PROJECT_PATH | sed 's/\//\\\//g')\/$CI_PROJECT_NAME\/$(echo "${APPS_PATH}/${APP}" | sed 's/\//\\\//g'):${TAG_VERSION}/" ${DEPLOYMENT_FILE}

          echo -e "${LOG_HEADER}Pushing to the GitOps repository...${LOG_FOOTER}"
          if [ "$DRY_RUN" = "true" ]
          then
            echo -e "${LOG_HEADER}--- Dry Run mode: GitOps 레포지토리에 반영 생략 됨...${LOG_FOOTER}"
          else
            git add ${DEPLOYMENT_FILE}
            git commit -m "[ci skip] ${CI_PROJECT_NAME}: GITOPS RESOURCES - UPDATED at `date +'%Y-%m-%d %H:%M:%S'` "
            git push ${GITOPS_URL} HEAD:main
          fi
          echo "Deployed tag version":"${TAG_VERSION}"
        fi
      done
#    - DEPLOYMENT_FILE="${MANIFESTS_PATH}/${APPS_PATH}/${APP}/deployment.yml"
#    - sed -i "s/^\(          image\s*:\s*\).*\$/\1$IMAGE_REGISTRY\/$(echo $CI_PROJECT_PATH | sed 's/\//\\\//g')\/$CI_PROJECT_NAME:${TAG_VERSION}/" ${DEPLOYMENT_FILE}
#
#    - echo -e "${LOG_HEADER}Pushing to the GitOps repository...${LOG_FOOTER}"
#
#    - |
#      if [ "$DRY_RUN" = "true" ]
#      then
#        echo -e "${LOG_HEADER}--- Dry Run mode: GitOps 레포지토리에 반영 생략 됨...${LOG_FOOTER}"
#      else
#        git add ${DEPLOYMENT_FILE}
#        git commit -m "[ci skip] GITOPS RESOURCES - UPDATED at `date +'%Y-%m-%d %H:%M:%S'` "
#        git push ${GITOPS_URL} HEAD:main
#      fi
#    - echo "Deployed tag version":"${TAG_VERSION}"
  rules:
    - if: $ENVIRONMENT == "dev"
      when: on_success
    - if: $ENVIRONMENT == "prd"
      when: on_success

#version:
#  stage: version
#  script:
#    - echo -e "${LOG_HEADER}--- 버전 확인 중...${LOG_FOOTER}"
#    - |
#      for WORKSPACE in $(echo ${WORKSPACES} | sed 's/,/ /g')
#      do
#        if [ -n ${WORKSPACE} ]
#        then
#          yarn workspace ${WORKSPACE} info --json | tail -n +2 | sed -e '$ d' > info.json
#          if [ -s info.json ]
#          then
#            VERSION=$(node -p "require('./info.json').data.versions.filter(function(element){return element.substr(0, 3) == \"${BASE_VERSION}\".substr(0, 3);}).reverse()[0]")
#            if [ "${VERSION}" = "undefined" ]; then {VERSION}=${BASE_VERSION}-0; fi
#          else
#            VERSION=${BASE_VERSION}-0
#          fi
#
#          yarn workspace ${WORKSPACE} version --no-git-tag-version --new-version ${VERSION}
#          VERSION=$(yarn workspace ${WORKSPACE} versions | grep ${WORKSPACE} | cut -d "'" -f4)
#          echo "CURRENT VERSION":"${WORKSPACE}":"${VERSION}"
#
#          if [ "$ENVIRONMENT" = "dev" ]
#          then
#            yarn workspace ${WORKSPACE} version --no-git-tag-version --new-version ${VERSION}
#            yarn workspace ${WORKSPACE} version --no-git-tag-version --prerelease
#          fi
#          if [ "$ENVIRONMENT" = "prd" ]
#          then
#            yarn workspace ${WORKSPACE} version --no-git-tag-version --new-version ${VERSION}
#            yarn workspace ${WORKSPACE} version --no-git-tag-version --patch
#          fi
#
#          VERSION=$(yarn workspace ${WORKSPACE} versions | grep ${WORKSPACE} | cut -d "'" -f4)
#          echo "NEW VERSION":"${WORKSPACE}":"${VERSION}"
#        fi
#      done
#  artifacts:
#    paths:
#      - "dramas/**/dist/"
#      - "apps/**/dist/"
#      - "dramas/**/package.json"
#      - "apps/**/package.json"
#    expire_in: 10 mins
#  rules:
#    - if: $ENVIRONMENT == "dev"
#      when: on_success
#    - if: $ENVIRONMENT == "prd"
#      when: manual

#publish:
#  stage: publish
#  script:
#    - echo -e "${LOG_HEADER}Publishing package...${LOG_FOOTER}"
#    - |
#      if [ "$DRY_RUN" = "true" ]
#      then
#        echo -e "${LOG_HEADER}--- Dry Run mode: 퍼블리쉬 생략 됨...${LOG_FOOTER}"
#      else
#        echo -e "\n${NPMRC_AUTH_NARAWAY}" >> .npmrc
#        echo -e "${NPMRC_AUTH_PROJECT}" >> .npmrc
#        echo -e "${NPMRC_AUTH_PRIVATE}" >> .npmrc
#
#        for WORKSPACE in $(echo ${WORKSPACES} | sed 's/,/ /g')
#        do
#          if [ -n ${WORKSPACE} ] && [[ ${WORKSPACE} != *app* ]]
#          then
#            VERSION=$(yarn workspace ${WORKSPACE} versions | grep ${WORKSPACE} | cut -d "'" -f4)
#            yarn workspace ${WORKSPACE} publish --new-version ${VERSION}
#          fi
#        done
#      fi
#    - echo "PUBLISHED MODULES":"${WORKSPACES}"
#  rules:
#    - if: $ENVIRONMENT == "dev"
#      when: on_success
#    - if: $ENVIRONMENT == "prd"
#      when: on_success
